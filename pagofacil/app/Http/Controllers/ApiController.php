<?php namespace Pagofaciltest\Http\Controllers;

use Pagofaciltest\Models\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Pagination\Paginator;
/**
 * @Controller(prefix="Api")
 */
class ApiController extends Controller {

  /**
   * Show the Index Page
   * @Get("/")
   */
  public function Index()
  {
    echo "Aqui Abra información Acerca de WS"; exit();
    return view('index');
  }


  /**
   * Show the Index Page
   * @Get("/Usuarios/{pagina}/{Register}", where={"pagina": "[0-9]+","Register": "[0-9]+"})
   */
  public function Usuarios($pagina=1, $Register=10)
  {
    try
    {
      Paginator::currentPageResolver(function() use ($pagina) {
          return $pagina;
      });
      $Usuarios= DB::table('users')
      ->select("email","name","id")
      ->where("activo","=","1")
      ->paginate($Register);
      return response()->Json([
        "msg"=> "Success",
        "Usuarios"=>$Usuarios
        ],200);
    }catch(\Exception $ex){
      return response()->Json([
        "msg"=> "Fail:".$ex->getMessage(),
        "Usuarios"=>null
        ],400);
    }
  }

  /**
   * Show the Index Page
   * @Get("/Usuario/{id}", where={"id": "[0-9]+"})
   */
  public function Usuario($id)
  {
      $Usuario= User::find($id);
      
      if($Usuario==null){
        return "{}";
      }
    return response()->Json([
      "msg"=> "Success",
      "Usuario"=>$Usuario->ToArray()
      ],200);
  }

  /**
   * Show the Index Page
   * @Post("/Usuario")
   */
  public function CrearUsuario(Request $request)
  {
      $Empresa= Empresa::find($id);
    return response()->Json([
      "msg"=> "Success",
      "Licencias"=>$Empresa->ToArray()
      ],200);
  }

  /**
   * Show the Index Page
   * @Put("/Usuario/{id_usuario}", where={"id_usuario": "[0-9]+"})
   */

  public function ActualizarUsuario($id_usuario,Request $request){

    $oldUsuario = User::find($id_usuario);

    if(!$oldUsuario instanceof User){
      return response()->json(['success' => false,"id"=>null,"msg"=>"Error no existe User con id:$id_usuario"], 400);
    }

    if($oldUsuario->activo!=1){
      return response()->json(['success' => false,"id"=>null,"msg"=>"Error User Inactivo id:$id_usuario"], 400);
    }
    $allData = $request->all();
    if(!isset($allData['data'])){
      return response()->json(['success' => false,"id"=>null,"msg"=>"Error no Se recibieron la informacion"], 400);
    }
    $newObject =(object) $allData['data'];
    if(!isset($newObject->name,$newObject->email))
    {
      return response()->json(['success' => false,"id"=>null,"msg"=>"información no tiene la estructura correcta"], 400);
    }
    if($newObject->email==null or $newObject->email=="")
    {
      return response()->json(['success' => false,"id"=>null,"msg"=>"información no tiene tipo de Usuario definido o esta vacio"], 400);  
    }
    try
    {
      $oldUsuario->email=$newObject->email;
      $oldUsuario->name =$newObject->name;
      if($oldUsuario->save()) {
        return response()->json(['success' => true,"id"=>$newObject->id,"data"=>$newObject,"msg"=>"Guardado correctamente"], 200);
      }else
      {
        return response()->json(['success' => false,"id"=>null,"msg"=>"Error al guardar los datos"], 400);
      }
    }
    catch(\Exception $ex)
    {
      return response()->json(['success' => false,"id"=>null,"msg"=>"Error al guardar: ".$ex->getMessage()], 400);
    }
    
  }

  /**
   * Show the Index Page
   * @Delete("/Usuario/{id_usuario}", where={"id_usuario": "[0-9]+"})
   */

  public function EliminarUsuario($id_usuario){

    $oldUsuario = User::find($id_usuario);

    if(!$oldUsuario instanceof User){
      return response()->json(['success' => false,"id"=>null,"msg"=>"Error no existe Anexo con id:$id_usuario"], 400);
    }

    if($oldUsuario->activo!=1){
      return response()->json(['success' => false,"id"=>null,"msg"=>"Error Anexo Ya estaba Eliminado id:$id_usuario"], 400);
    }
    
    try
    {
      $oldUsuario->activo = 0;
      if($oldUsuario->save()) {
        return response()->json(['success' => true,"id"=>$oldUsuario->id,"data"=>'',"msg"=>"Eliminado correctamente"], 200);
      }else
      {
        return response()->json(['success' => false,"id"=>null,"msg"=>"Error al Eliminar los datos"], 400);
      }
    }
    catch(\Exception $ex)
    {
      return response()->json(['success' => false,"id"=>null,"msg"=>"Error al Borrar: ".$ex->getMessage()], 400);
    }
    
  }

}