<?php

namespace Pagofaciltest\Http\Controllers;

use Pagofaciltest\Http\Requests;
use Illuminate\Http\Request;
/**
 * @Controller(prefix="Home")
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @Get("/")
     * @return Response
     */
    public function index()
    {
        return view('home');
    }
}
