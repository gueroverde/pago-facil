<?php

namespace Pagofaciltest\Http\Controllers;

use Pagofaciltest\Http\Requests;
use Illuminate\Http\Request;
/**
 * @Controller(prefix="Usuario")
 */
class UsuariosController extends Controller
{
   

      /**
   * Show the Index Page
   * @Get("/{id}", where={"id": "[0-9]+"})
   */
  public function Usuario($id)
  {
      return view('Usuarios/ver',["id_usuario"=>$id]);

  }
}
