function getUsuario($id_usuario) {
    var objetoUsuario=null;
    $.ajax({
        method: "GET",
        url: "/Api/Usuario/" + $id_usuario,
        cache: false,
        contentType: 'application/json',
        data: {}
    })
        .done(function(data) {
            $('#usuarioEmail').val(data.Usuario.email);
            $('#usuarioNombre').val(data.Usuario.name);
            
    });
}

function fillFormUsuarioActualizar(objeto){
        $('#usuarioEmail').val(objeto.email);
        $('#usuarioNombre').val(objeto.name);
                    
    if($("#MSG").hasClass('alert-success')){
        $("#MSG").removeClass('alert-success');
    }
    if(!$("#MSG").hasClass('alert-danger')){
        $("#MSG").addClass('alert-danger');
    }
    $("#MSG").html('error No existe registro o fue eliminado ');
    $("#frmActualizarUsuario").hide();
        
}

$("#btnActualizarUsuario").on("click",function(){

    var id_usuario=$("#txtIdUsuario").val();
    
    var JsonForm=$("#frmActualizarUsuario").serializeObject();
    $.ajax({
        type:"PUT",
        url: "/Api/Usuario/"+id_usuario,
        cache: false,
        contentType: 'application/json',
        data:JSON.stringify({"data":JsonForm.Usuario})
    })
    .done(function(msg){
     if($("#MSG").hasClass('alert-danger')){
        $("#MSG").removeClass('alert-danger');
     }
     if(!$("#MSG").hasClass('alert-success')){
        $("#MSG").addClass('alert-success');
     }
     $("#MSG").text('Se actualizo correctamente');
     
     //alert alert-success
    }).error(function(error){
        if($("#MSG").hasClass('alert-success')){
        $("#MSG").removeClass('alert-success');
         }
         if(!$("#MSG").hasClass('alert-danger')){
            $("#MSG").addClass('alert-danger');
         }
         $("#MSG").html('error; ' + error.statusText+ "Mensaje:"+error.responseText + "Codigo:"+ error.status);
         
            
    });

});

$("#btnEliminarUsuario").on("click",function(){

    if(confirm("Deseas Eliminar este Usuario"))
    {
        var id_usuario=$("#txtIdUsuario").val();
        $.ajax({
            type:"DELETE",
            url: "/Api/Usuario/"+id_usuario,
            cache: false,
            contentType: 'application/json',
            data:JSON.stringify({})
        })
        .done(function(msg){
        alert("se Elimino correctamente ");
        window.location='/Home';
        })
        .error(function(error){
            if($("#MSG").hasClass('alert-success')){
            $("#MSG").removeClass('alert-success');
             }
             if(!$("#MSG").hasClass('alert-danger')){
                $("#MSG").addClass('alert-danger');
             }
             $("#MSG").html('error; ' + error.statusText+ "Mensaje:"+error.responseText + "Codigo:"+ error.status);
            
                
            });
    }
});


