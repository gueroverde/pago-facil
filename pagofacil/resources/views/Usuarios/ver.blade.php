@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div id="MSG" class="alert" role="alert"></div>
                    <form id="frmActualizarUsuario">
                        <div class="span3">
                            <label>Nombre:</label>
                            <input type="text" id="usuarioNombre" name="Usuario[name]" />
                        </div>

                        <div class="span3">
                            <label>Email:</label>
                            <input type="email" id="usuarioEmail" name="Usuario[email]" />
                        </div>

                        <div class="span3">
                            <input type="button" id="btnActualizarUsuario" class="btn btn-success" name="Usuario[Actualizar]" value="Actualizar" />
                            <input type="button" id="btnEliminarUsuario" class="btn btn-danger" name="Usuario[Eliminar]" value="ELiminar" />
                            <input type="hidden" id="txtIdUsuario" name="Usuario[id]" value="{{$id_usuario}}" />
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/Pagofaciltest/Usuarios/ver.js"></script>
<script type="text/javascript">
getUsuario(1);
</script>
@endsection