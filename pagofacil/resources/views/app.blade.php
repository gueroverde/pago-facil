<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Comisión de Anuncios de GUadalajara</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/js/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet">
	<link href="{{ asset('/js/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
	

	<script type="text/javascript" src="{{ asset('/js/catalogos.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('/js/busquedas.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('/js/blockui/jquery.blockUI.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('/js/bootstrap-modal/js/bootstrap-modalmanager.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('/js/bootstrap-modal/js/bootstrap-modal.js') }}" ></script>
	<script src="{{ asset('/js/jquery.serialize-object.min.js') }}"></script>
	 <script src="{{ asset('/js/jquery-validation/src/core.js') }}"></script>
	 <script src="{{ asset('/js/utilidades.js') }}"></script>
		<style>
		  .ui-autocomplete-loading {
		    background: white url("https://jqueryui.com/resources/demos/autocomplete/images/ui-anim_basic_16x16.gif") right center no-repeat;
		  }
  </style>
	<script type="text/javascript">
		$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	</script>

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button"  class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img width="50" height="30" src="{{ asset('/img/logogdl.png') }}" /> </a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/') }}">Home</a></li>
					<li><a href="{{ url('establecimientos') }}">Establecimientos</a></li>
					<li><a href="{{ url('tipoAnexos') }}">Tipos de Anexos</a></li>
					<li><a href="{{ url('tipoAnuncios') }}">Tipos de Anuncios</a></li>
					<li><a href="{{ url('giros') }}">Giros</a></li>


				</ul>



				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

</body>
</html>
