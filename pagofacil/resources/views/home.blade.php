@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    Tabla
                    <table id="tblUsuarios"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/Pagofaciltest/Home.js"></script>
<script type="text/javascript">
paginarHome(1,10);
</script>
@endsection
