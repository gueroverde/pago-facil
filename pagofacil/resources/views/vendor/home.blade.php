@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<form id="formHomePendientes">	
						<h1>Anuncios Que estan pendientes</h1>
						<select name="filter[tipoAnexo]" id='filterTipoAnexo' >
							<option value="">Selecciona el Tipo de Anexo</option>
							@foreach ($tipoAnexos as $tipoAnexo)
								<option value="{{$tipoAnexo->id_tipo_anexo}}">{{$tipoAnexo->tipo_anexo}}</option>
							@endforeach
						</select>
					</form>

				</div>
				<div id="divTablaHome">Aqui va a ir la tabla

				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/js/app/home.js"></script>
<script type="text/javascript">
paginarHome(1,10);
</script>
@endsection
